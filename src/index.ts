
import { ApiService } from './api.service';
import { updateFavouritePanel, resetInitialUi } from './helpers/page-creator';

declare type LoadMovieFunc = () => Promise<void>;

let currentPage = 1;
let selectedLoadFunc = getPopularMovies;


export async function render(): Promise<void> {
    await getPopularMovies();

    const topRatedBtn = document.getElementById('top_rated');
    if (topRatedBtn) {
        topRatedBtn.addEventListener("click", async event => {
            currentPage = 1;
            selectedLoadFunc = getTopRatedMovies;
            await getTopRatedMovies();
        })
    }

    const upcomingBtn = document.getElementById('upcoming');
    if (upcomingBtn) {
        upcomingBtn.addEventListener("click", async event => {
            currentPage = 1;
            selectedLoadFunc = getUpcomingMovies;
            await getUpcomingMovies();
        })
    }

    const popularBtn = document.getElementById('popular');
    if (popularBtn) {
        popularBtn.addEventListener("click", async event => {
            currentPage = 1;
            selectedLoadFunc = getPopularMovies;
            await getPopularMovies();
        })
    }


    const searchBtn = document.getElementById('submit');
    if (searchBtn) {
        searchBtn.addEventListener('click', async event => {
            currentPage = 1;
            selectedLoadFunc = searchMovie;
            await searchMovie();
        })
    }

    const loadMoreBtn = document.getElementById('load-more');
    if (loadMoreBtn) {
        loadMoreBtn.addEventListener('click', async event => {
            currentPage++;
            await selectedLoadFunc();
        })
    }

    const favMenuBtn = document.getElementsByClassName('navbar-toggler')[0];
    if(favMenuBtn){
        favMenuBtn.addEventListener('click', event =>{
            updateFavouritePanel([]);
        })
    }

}

async function getPopularMovies() {
    const apiService = new ApiService();
    const res = await apiService.getPopularMovies(currentPage);
    const movies = res.results;

    const shouldClean = currentPage === 1;
    resetInitialUi(movies, shouldClean);

}

async function getUpcomingMovies() {
    const apiService = new ApiService();
    const res = await apiService.getUpcomingMovies(currentPage);
    const movies = res.results;

    const shouldClean = currentPage === 1;
    resetInitialUi(movies, shouldClean);

}

async function getTopRatedMovies() {
    const apiService = new ApiService();
    const res = await apiService.getTopRatedMovies(currentPage);
    const movies = res.results;

    const shouldClean = currentPage === 1;
    resetInitialUi(movies, shouldClean);
}

async function searchMovie() {
    const apiService = new ApiService();
    const searchBox = document.getElementById('search') as HTMLInputElement;
    const res = await apiService.searchMovie(searchBox.value, currentPage);
    const movies = res.results;

    const shouldClean = currentPage === 1;
    resetInitialUi(movies, shouldClean);
}



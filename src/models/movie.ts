export interface IMovie {

    id: number,
    overview: string,
    poster_path: string, //"/4q2hz2m8hubgvijz8Ez0T2Os2Yv.jpg"
    release_date: string, //"2021-05-21"
    title: string,
    isLiked?: boolean
}

export interface IApiMovie {
    adult: boolean,
    backdrop_path: string, //"/z2UtGA1WggESspi6KOXeo66lvLx.jpg"
    genre_ids: number[],
    id: number,
    original_language: string, //"en"
    original_title: string,
    overview: string,
    popularity: number, //5337.541
    poster_path: string, //"/4q2hz2m8hubgvijz8Ez0T2Os2Yv.jpg"
    release_date: string, //"2021-05-21"
    title: string,
    video: boolean,
    vote_average: number, //7.4
    vote_count: number, //461

}
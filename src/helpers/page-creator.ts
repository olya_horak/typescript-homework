import { IMovie } from './../models/movie';
import { createElement, createElementNs } from "./dom-helper";

const imgBasePath = 'https://image.tmdb.org/t/p/original';


export function createMovieTile(movie: IMovie): HTMLElement {
    const div = createElement({ tagName: 'div', className: 'col-lg-3 col-md-4 col-12 p-2' });
    const innerDiv = createElement({ tagName: 'div', className: 'card shadow-sm' });
    const img = createElement({ tagName: 'img', attributes: { 'src': imgBasePath + movie.poster_path } });

    const heart = createElementNs('http://www.w3.org/2000/svg', {
        tagName: 'svg',
        attributes: { 'stroke': 'red', 'fill': 'red', 'width': '50', 'height': '50', 'viewBox': `0 -2 18 22` },
        className: 'bi bi-heart-fill position-absolute p-2'
    });
    const path = createElementNs('http://www.w3.org/2000/svg', { tagName: 'path', attributes: { 'fill-rule': 'evenodd', 'd': `M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z` } })
    fillHeart(heart,movie);
    const textDiv = createElement({ tagName: 'div', className: 'card-body' });
    const text = createElement({ tagName: 'p', className: 'card-text truncate' });
    const style = createElement({ tagName: 'div', className: 'd-flex justify-content-between align-items-center' });
    const date = createElement({ tagName: 'small', className: 'text-muted' });
    text.innerText = movie.overview;
    date.innerText = movie.release_date;


    style.appendChild(date);
    textDiv.appendChild(text);
    textDiv.appendChild(style);

    innerDiv.appendChild(img);
    heart.appendChild(path);
    innerDiv.appendChild(heart);
    innerDiv.appendChild(textDiv);


    div.appendChild(innerDiv);
    return div;

}

export function fillHeart(heart: Element, movie: IMovie): void {
    if (movie.isLiked) {
        heart.setAttribute('fill', 'red')
    }
    else { heart.setAttribute('fill', 'white') };
}

export function updateFavouritePanel(likedMovies: IMovie[]):void{
    const favHolder = document.getElementById('favorite-movies');
    if(favHolder){
        favHolder.innerHTML='';

        likedMovies.forEach(it => {
            const div = createMovieTile(it);
            favHolder.appendChild(div);
        });
       
    }
    
}

export function showFavouriteMovies (movie: IMovie):void{
    if (movie.isLiked){
    // return movies:[] = movies[]
    }
}

export function updateBanner(movie: IMovie): void {
    const bannerName = document.getElementById('random-movie-name');
    if (bannerName != null) {
        bannerName.innerText = movie.title;
    }
    const banner = document.getElementById('random-movie-description');
    if (banner != null) {
        banner.innerText = movie.overview;
    }
}

export function resetInitialUi(movies: IMovie[], shouldClean = true): void {
    const container = document.getElementById('film-container');
    if (!container) return;
    if (shouldClean == true) {
        container.innerHTML = '';
    }

    movies.forEach(it => {
        const div = createMovieTile(it);
        container.appendChild(div);
    });
    const randomFilmIndex = Math.round(Math.random() * (movies.length - 1));
    updateBanner(movies[randomFilmIndex]);
}
export interface ICreateElementArgs {
    tagName: string,
    className?: string,
    attributes?: Record<string, string>
}

export function createElement(args: ICreateElementArgs): HTMLElement {
    const element = document.createElement(args.tagName);

    if (args.className) {
        const classNames = args.className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    if (args.attributes) {
        Object.keys(args.attributes).forEach((key) =>
            element.setAttribute(key, (args.attributes as any)[key]));
    }

    return element;
}

export function createElementNs(ns: string, args: ICreateElementArgs): Element {
    const element = document.createElementNS(ns, args.tagName);

    if (args.className) {
        const classNames = args.className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    if (args.attributes) {
        Object.keys(args.attributes).forEach((key) =>
            element.setAttribute(key, (args.attributes as any)[key]));
    }

    return element;
}
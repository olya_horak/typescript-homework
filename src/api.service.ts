import { IApiResponse } from './models/api';
// https://api.themoviedb.org/3/movie/76341?api_key=<<api_key>>
import axios, { AxiosRequestConfig } from 'axios';
import { IApiMovie, IMovie } from './models/movie';

const BASE_URL = 'https://api.themoviedb.org/3/';
const API_KEY = 'd9a165cae9c0b8823a4744999ef2c15b';
const ACCESS_TOKEN = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkOWExNjVjYWU5YzBiODgyM2E0NzQ0OTk5ZWYyYzE1YiIsInN1YiI6IjYwZDZlMTFmODgwNTUxMDA3NGM3NGQ5NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.7DnKIaOH6A1vz_KK7on693y7xGNH0pQP-op-VTTt6LI';

const HEADERS = {
    'Authorization': `Bearer ${ACCESS_TOKEN}`,
    'Content-Type': `application/json;charset=utf-8`,
}

const CONFIG: AxiosRequestConfig = {
    headers: HEADERS,
    baseURL: BASE_URL
};


export class ApiService {
    public async getPopularMovies(page = 1): Promise<IApiResponse<IMovie>> {
        const res = await axios.get<IApiResponse<IApiMovie>>(`/movie/popular?page=${page}`, CONFIG);

        return this.cutOffUnusedFields(res.data);
    }

    public async getUpcomingMovies(page = 1): Promise<IApiResponse<IMovie>> {
        const res = await axios.get<IApiResponse<IApiMovie>>(`/movie/upcoming?page=${page}`, CONFIG);

        return this.cutOffUnusedFields(res.data)
    }

    public async getTopRatedMovies(page = 1): Promise<IApiResponse<IMovie>> {
        const res = await axios.get<IApiResponse<IApiMovie>>(`/movie/top_rated?page=${page}`, CONFIG);

        return this.cutOffUnusedFields(res.data)
    }

    public async searchMovie(query: string, page = 1): Promise<IApiResponse<IMovie>> {
        const res = await axios.get<IApiResponse<IApiMovie>>(`/search/movie?query=${query}&page=${page}`, CONFIG);

        return this.cutOffUnusedFields(res.data)
    }


    public cutOffUnusedFields(original: IApiResponse<IApiMovie>): IApiResponse<IMovie> {
        const result: IApiResponse<IMovie> = {
            page: original.page,
            total_pages: original.total_pages,
            total_results: original.total_results,
            results: original.results.map(x => {
                return {
                    id: x.id,
                    overview: x.overview,
                    poster_path: x.poster_path,
                    release_date: x.release_date,
                    title: x.title,
                    isLiked: false
                }
            })
        };
        return result;
    }


}

